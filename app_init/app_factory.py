from flask import Flask
from extension.extension import db,ma,migrate,jwt
from logging.config import dictConfig
import logging, sys


settings = {
    "dev" : "settings.devsettings.DevSettings",
    "prod" : "settings.prodsettings.ProdSettings"
}

def get_settings(settings_name):
    if settings.get(settings_name):
        return settings.get(settings_name)
    else:
        return Exception("Setting name you select %s isn't supported" % settings_name)

dictConfig({
    'version': 1,
        
    'formatters': {'default': {
        'format': '[%(asctime)s.%(msecs)03d] %(levelname)s %(name)s:%(funcName)s:%(message)s  %(module)s: %(message)s'
    }},
    'handlers': {'wsgi': {
        'class': 'logging.FileHandler',
        # 'stream': 'ext://flask.logging.wsgi_errors_stream',
        'formatter': 'default',
        'filename' : 'test.log'
        },
        'custom_handler':{
            'class' : 'logging.FileHandler',
            'filename' : 'test.log'
        }
    },
    'root': {
        'level': 'INFO',
        'handlers': ['wsgi']
    }
})

def create_app(settings_name):
    app = Flask(__name__)
    Logger = logging.getLogger(__name__)
    handler = logging.StreamHandler(sys.stdout)
    app.logger.addHandler(handler)
    db.init_app(app)
    ma.init_app(app)
    app.config['JWT_SECRET_KEY'] = 'test12345'
    jwt.init_app(app)
    migrate.init_app(app, db)
    settings_obj = get_settings(settings_name)
    app.config.from_object(settings_obj)

    ctx = app.app_context()
    ctx.push()

    return app